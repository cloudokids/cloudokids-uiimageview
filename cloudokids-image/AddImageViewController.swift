//
//  AddImageViewController.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/7/29.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

class AddImageViewController: UIViewController {
    
    //storyboard
    @IBOutlet weak var addImageCollectionView: UICollectionView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var gestureView: UIView!
    @IBOutlet weak var ratioButton: UIButton!
    
    //var
    let gesture = GestureRecognizer()
    var selectImageView: OperationImageView?
    var allImageViews: [OperationImageView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewController()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        hiddenView()
    }

    func setupViewController() {
        automaticallyAdjustsScrollViewInsets = false
        
        setupGestureRecognizer()
        
        Photos.fetch{ assets in }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddImageViewController.getImage(_:)), name: "image", object: nil)
    }
    
    func imageCreator(image: UIImage) -> OperationImageView {
        let image = ModificationImage.imageCreator(image)
        let imageView = OperationImageView(image: image)
        imageView.backgroundView = backgroundView
        imageView.delegate = self
        
        imageView.alpha = 0
        UIView.animateWithDuration(THEME_ANIMATION_DURATION, animations: {
            imageView.alpha = 1
        })
        addImageCollectionView.reloadData()
        backgroundView.drawRect(backgroundView.frame)
        
        return imageView
    }
    
    func selectImageView(imageView: OperationImageView) {
        if let image = selectImageView {
            image.layer.borderWidth = 0
        }
        imageView.layer.borderWidth = 2
        imageViewBorderColor(imageView)
        gesture.isLocking = imageView.isLocking
        selectImageView = imageView
        gesture.view = selectImageView
    }
    
    func imageViewBorderColor(imageView: OperationImageView) {
        if imageView.isLocking {
            imageView.layer.borderColor = THEME_COLOR_BLACK.CGColor
        } else {
            imageView.layer.borderColor = THEME_COLOR_RED.CGColor
        }
    }
    
    func getImage(notification: NSNotification) {
        let imageDic = notification.object as? [String:UIImage]
        if let dict = imageDic, im = dict["image"] {
            let imageView = imageCreator(im)
            allImageViews.append(imageView)
            multiplyImage(allImageViews)
            imageViewTransparent(imageView)
            selectImageView(imageView)
        }
    }
    
    func multiplyImage(imageViews: [OperationImageView]) {
        let rect = CGRect(x: 0, y: 0, width: backgroundView.bounds.size.width, height: backgroundView.bounds.size.height)
        
        UIGraphicsBeginImageContextWithOptions(backgroundView.bounds.size, true, 0)
        let context = UIGraphicsGetCurrentContext()
        
        for imageView in imageViews {
            CGContextSetFillColorWithColor(context, UIColor.whiteColor().CGColor)
            CGContextFillRect(context, rect)
            imageView.image?.drawInRect(rect, blendMode: .Multiply, alpha: 1)
        }
        UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    func setupGestureRecognizer() {
        gesture.viewDelegate = self
        gesture.addPanGestureRecognizer(gestureView)
        gesture.addPinchGestureRecognizer(gestureView)
        gesture.addRotationGestureRecognizer(gestureView)
        gesture.addTapGestureRecognizer(gestureView)
    }
    
    func imageViewTransparent(imageView: UIImageView) {
        for imageV in allImageViews {
            if imageV != imageView {
                UIView.animateWithDuration(THEME_ANIMATION_DURATION, animations: {
                    imageV.alpha = THEME_VIEW_ALPHA_TRANSLUCENT
                    })
            }
        }
    }
    
    func hiddenView() {
        if let _ = gesture.view {
            addImageCollectionView.hidden = false
            ratioButton.hidden = true
            gestureView.hidden = false
        } else {
            addImageCollectionView.hidden = true
            ratioButton.hidden = false
            gestureView.hidden = true
        }
    }
    
    @IBAction func confirm(sender: UIBarButtonItem) {
        if allImageViews.count > 0 {
            selectImageView?.layer.borderWidth = 0
            ModificationImage.saveImages(allImageViews)
            allImageViews.removeAll()
        }
    }
    
    @IBAction func add(sender: UIBarButtonItem) {
        let destinationViewController = storyboard?.instantiateViewControllerWithIdentifier("ListPhotosViewController") as! ListPhotosViewController
        navigationController?.pushViewController(destinationViewController, animated: true)
    }
    
    @IBAction func ratio(sender: UIButton) {
        ModificationImage.getAutomaticRatioView(allImageViews, backgroundView: backgroundView)
        sender.userInteractionEnabled = false
    }
    
}

