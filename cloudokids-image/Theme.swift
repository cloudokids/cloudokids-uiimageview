//
//  Theme.swift
//  PhotoCamera
//
//  Created by 浦源 on 16/6/30.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

//Color
let THEME_COLOR_BLACK = UIColor.blackColor()
let THEME_COLOR_CLEAR = UIColor.clearColor()
let THEME_COLOR_WHITE = UIColor.whiteColor()
let THEME_COLOR_RED = UIColor.redColor()
let THEME_COLOR_LIGHT = UIColor(red: 240.0 / 255.0, green: 240.0 / 255.0, blue: 240.0 / 255.0, alpha: 1)
//
////Collection view
//let THEME_COLLECTIONVIEW_WIDTH_SPACING = CGFloat(2.0)
//let THEME_COLLECTIONVIEW_HEIGNT_SPACING = CGFloat(2.0)
//let THEME_COLLTECTION_FILTER_LABEL_HEIGHT = CGFloat(40.0)
//let THEME_COLLECTION_BORDER_WIDTH = CGFloat(3)
//
////Button
//let THEME_BUTTON_CAMERA_WIDTH = CGFloat(60.0)
//let THEME_BUTTON_CAMERA_HEIGHT = CGFloat(60.0)
//
////View
let THEME_CROPVIEW_BORDER_WIDTH = CGFloat(2.0)
let THEME_ARROW_MINIMUM_SPACE = CGFloat(20.0)
let THEME_ARROW_WIDTH = CGFloat(25.0)
let THEME_ARROW_HEIGHT = CGFloat(22.0)
let THEME_VIEW_MINIMUM_SPACE = CGFloat(20)
let THEME_VIEW_ALPHA_TRANSLUCENT = CGFloat(0.6)
//let THEME_VIEW_CAMERA_FOCUS_MARGIN = CGFloat(70.0)
//let THEME_VIEW_CAMERA_FOCUS_BORDER_WIDTH = CGFloat(1.0)
//let THEME_VIEW_CAMERA_FOCUS_TRANSFORM = CGFloat(1.25)
//let THEME_VIEW_CAMERA_VIEW_ALPHA = CGFloat(0.6)
//let THEME_VIEW_SEGMENTEDCONTROL_HEIGHT = CGFloat(40.0)
//
////Navgation
//let THEME_NAVGATION_FRAME_MAXY = CGFloat(64.0)
//
////Animation duration
let THEME_ANIMATION_DURATION = 0.25
let THEME_ANIMATION_FAST = 0.1
//let THEME_ANIMATION_CAMERA_DURATION = 0.5
//let THEME_ANIMATION_CAMERA_TYPE = "oglFlip"
//
////Font
//let THEME_FONT = "Avenir Next"
//
////Font Size
//let THEME_FONT_XL = CGFloat(18.0)
//let THEME_FONT_L = CGFloat(16.0)
//let THEME_FONT_M = CGFloat(14.0)
//let THEME_FONT_S = CGFloat(12.0)
//let THEME_FONT_XS = CGFloat(10.0)
//
////TableView
//let THEME_TABLEVIEW_HEADER_SECTION_HEIGHT = CGFloat(10.0)
//let THEME_TABLEVIEW_FOOTER_SECTION_HEIGHT = CGFloat(1.0)
//
////Filter
//let FILTER_LABEL_HEIGHT = CGFloat(40.0)
//
////TagView
//let THEME_TAGVIEW_ADJUST_MARGIN = CGFloat(15.0)
//
////倍数 x2
//let THEME_DOUBLE_MULTIPLE = CGFloat(2.0)
//let THEME_LIMITRATIO = CGFloat(3.0)
//
////iPhone6 6s
//let THEME_SCREEN_BOUNDS_SIZE_WIDTH = CGFloat(375.0)
//let THEME_SCREEN_BOUNDS_SIZE_HEIGHT = CGFloat(667.0)