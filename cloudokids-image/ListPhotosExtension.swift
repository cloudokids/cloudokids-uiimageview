//
//  ListPhotosExtension.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/8/2.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

extension ListPhotosViewController: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ListPhotosCollectionViewCell", forIndexPath: indexPath) as! ListPhotosCollectionViewCell
        
        cell.imageView.clipsToBounds = true
        let asset = assets[indexPath.row]
        Photos.resolveAsset(asset, size: CGSize(width: 160, height: 240)) { image in
            if let image = image {
                cell.imageView.image = image
            }
        }
        
        return cell
    }
    
}

extension ListPhotosViewController: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let asset = assets[indexPath.row]
        let image = Photos.resolveAssets([asset])[0]
        let destinationViewController = storyboard?.instantiateViewControllerWithIdentifier("ClipImageViewController") as! ClipImageViewController
        destinationViewController.photoImage = image
        navigationController?.pushViewController(destinationViewController, animated: true)

    }
    
}

extension ListPhotosViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: view.bounds.width / 4, height: view.bounds.width / 4 - 2)
    }
    
}

class ListPhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
