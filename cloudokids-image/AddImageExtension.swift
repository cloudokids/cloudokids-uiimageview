//
//  AddImageExtension.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/8/5.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

extension AddImageViewController: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return OPERATION_STRINGS.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddImageCollectionViewCell", forIndexPath: indexPath) as! AddImageCollectionViewCell
        cell.operationImageView.alpha = 1
        cell.selected = true
        
        if let imageView = selectImageView {
            if imageView.isLocking && indexPath.row < 3 {
                cell.operationImageView.alpha = 0.5
                cell.selected = false
            }
            if allImageViews.count < 2 && indexPath.row < 5 && indexPath.row > 2 {
                cell.operationImageView.alpha = 0.5
                cell.selected = false
            } else {
                for i in 0 ..< backgroundView.subviews.count {
                    if backgroundView.subviews[i] as? OperationImageView == imageView {
                        if (i == 0 && indexPath.row == 4) || (i == backgroundView.subviews.count - 1 && indexPath.row == 3) {
                            cell.operationImageView.alpha = 0.5
                            cell.selected = false
                        }
                        break
                    }
                }
            }
        }
        
        cell.contentView.backgroundColor = THEME_COLOR_LIGHT
        cell.contentView.alpha = 0.9
        cell.operationImageView.image = UIImage(named: OPERATION_IMAGE_NAME[indexPath.row])
        cell.operationLabel.text = OPERATION_STRINGS[indexPath.row]
        
        return cell
    }
    
}

extension AddImageViewController: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            if let imageView = selectImageView {
                if !imageView.isLocking {
                    imageView.removeFromSuperview()
                    
                    for i in 0 ..< allImageViews.count {
                        if allImageViews[i] == imageView {
                            allImageViews.removeAtIndex(i)
                            removeView()
                            ratioButton.userInteractionEnabled = true
                            return
                        }
                    }
                }
            }
        case 1:
            if let imageView = selectImageView, let image = imageView.image {
                if !imageView.isLocking {
                    let flipImageOrientation = (image.imageOrientation.rawValue + 4) % 8
                    
                    if let cgimage = image.CGImage, orientation = UIImageOrientation(rawValue: flipImageOrientation) {
                        let flipImage = UIImage(CGImage:cgimage, scale:image.scale, orientation: orientation)
                        imageView.image = flipImage
                    }
                }
            }
        case 2:
            if let imageView = selectImageView {
                if let image = imageView.image {
                    let imageV = imageCreator(image)
                    imageV.frame = CGRect(x: imageView.frame.origin.x + THEME_VIEW_MINIMUM_SPACE, y: imageView.frame.origin.y, width: imageView.frame.width, height: imageView.frame.height)
                    allImageViews.append(imageV)
                    selectImageView(imageV)
                    imageViewTransparent(imageV)
                    addImageCollectionView.reloadData()
                    ratioButton.userInteractionEnabled = true
                }
            }
        case 3:
            if let imageView = selectImageView {
                backgroundView.bringSubviewToFront(imageView)
                addImageCollectionView.reloadData()
            }
        case 4:
            if let imageView = selectImageView {
                backgroundView.sendSubviewToBack(imageView)
                addImageCollectionView.reloadData()
            }
        case 5:
            if let imageView = selectImageView {
                if gesture.isLocking {
                    gesture.isLocking = false
                } else {
                    gesture.isLocking = true
                }
                if imageView.isLocking {
                    imageView.isLocking = false
                } else {
                    imageView.isLocking = true
                }
                imageViewBorderColor(imageView)
                addImageCollectionView.reloadData()
            }
        default:
            break
        }
    }
    
}

extension AddImageViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: view.bounds.width / CGFloat(OPERATION_STRINGS.count), height: view.bounds.width / CGFloat(OPERATION_STRINGS.count) + 20.0)
    }
    
}

extension AddImageViewController: OperationImageViewDelegate {
    
    func clickImage(imageView: OperationImageView) {
        selectImageView(imageView)
        hiddenView()
        imageViewTransparent(imageView)
        addImageCollectionView.reloadData()
        UIView.animateWithDuration(THEME_ANIMATION_FAST, animations: { [weak self] in
            if let weakSelf = self, imageView = weakSelf.selectImageView {
                imageView.transform = CGAffineTransformScale(imageView.transform, 0.95, 0.95)
            }
        }) { (_) in
            UIView.animateWithDuration(THEME_ANIMATION_FAST, animations: { [weak self] in
                if let weakSelf = self, imageView = weakSelf.selectImageView {
                    imageView.transform = CGAffineTransformScale(imageView.transform, 1/0.95, 1/0.95)
                }
                })
        }
    }
    
    func viewPictureChange() {
        ratioButton.userInteractionEnabled = true
    }
}

extension AddImageViewController: GestureRecognizerViewDelegate {
    
    func removeView() {
        if allImageViews.count > 0 {
            selectImageView?.layer.borderWidth = 0
            gesture.view = nil
            hiddenView()
            ratioButton.alpha = 0
            for imageV in allImageViews {
                UIView.animateWithDuration(THEME_ANIMATION_DURATION, animations: { [weak self] in
                    imageV.alpha = 1
                    if let weakSelf = self {
                        weakSelf.ratioButton.alpha = 0.9
                    }
                    })
            }
        }
    }
    
    func pictureChange() {
        ratioButton.userInteractionEnabled = true
    }
    
}

class AddImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var operationImageView: UIImageView!
    @IBOutlet weak var operationLabel: UILabel!
    
}
