//
//  ModificationImage.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/7/29.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

struct ModificationImage {
    
    static func imageCreator(image: UIImage) -> UIImage {
        
        let imageOrientation = image.imageOrientation
        
        var newImage: UIImage = image
        
        if imageOrientation != UIImageOrientation.Up {
            UIGraphicsBeginImageContext(newImage.size)
            newImage.drawInRect(CGRectMake(0, 0, newImage.size.width, newImage.size.height))
            newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        //convert white to transparent
        let imageTemp = UIImage(data: UIImageJPEGRepresentation(newImage, 1.0)!)!
        let rawImageRef: CGImageRef = imageTemp.CGImage!
        let colorMasking: [CGFloat] = [254, 255, 254, 255, 254, 255]
        UIGraphicsBeginImageContext(imageTemp.size)
        let maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking)
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height)
        CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0)
        CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, imageTemp.size.width, imageTemp.size.height), maskedImageRef)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
//        
//        UIGraphicsBeginImageContextWithOptions(result.size, false, UIScreen.mainScreen().scale)
//        let context = UIGraphicsGetCurrentContext()
//        CGContextSetShouldAntialias(context, true)
//        CGContextSetInterpolationQuality(context, CGInterpolationQuality.High)
//        result.drawInRect(CGRect(x: 0, y: 0, width: result.size.width, height: result.size.height))
//        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
        return result
    }
    
    static func saveImages(imageViews: [UIImageView]) {
        var minX: CGFloat = CGFloat.max
        var minY: CGFloat = CGFloat.max
        var maxX: CGFloat = CGFloat.min
        var maxY: CGFloat = CGFloat.min
        for imageView in imageViews {
            minX = imageView.frame.origin.x > minX ? minX : imageView.frame.origin.x
            minY = imageView.frame.origin.y > minY ? minY : imageView.frame.origin.y
            maxX = imageView.frame.maxX < maxX ? maxX : imageView.frame.maxX
            maxY = imageView.frame.maxY < maxY ? maxY : imageView.frame.maxY
        }
        
        var viewW: CGFloat = 0
        var viewH: CGFloat = 0
        if maxX - minX > maxY - minY {
            viewW = maxX - minX + 2 * THEME_VIEW_MINIMUM_SPACE
            viewH = maxX - minX + 2 * THEME_VIEW_MINIMUM_SPACE
        } else {
            viewW = maxY - minY + 2 * THEME_VIEW_MINIMUM_SPACE
            viewH = maxY - minY + 2 * THEME_VIEW_MINIMUM_SPACE
        }
        
        if viewW - 2 * THEME_VIEW_MINIMUM_SPACE < 300 {
            let proportion = 300 / (viewW - 2 * THEME_VIEW_MINIMUM_SPACE)
            viewW = 300 + 2 * THEME_VIEW_MINIMUM_SPACE
            viewH = 300 + 2 * THEME_VIEW_MINIMUM_SPACE
            
            for imageView in imageViews {
                imageView.bounds = CGRect(x: 0, y: 0, width: imageView.bounds.width * proportion, height: imageView.bounds.height * proportion)
                imageView.center = CGPoint(x: imageView.center.x * proportion, y: imageView.center.y * proportion)
                minX = imageView.frame.origin.x > minX ? minX : imageView.frame.origin.x
                minY = imageView.frame.origin.y > minY ? minY : imageView.frame.origin.y
                maxX = imageView.frame.maxX < maxX ? maxX : imageView.frame.maxX
                maxY = imageView.frame.maxY < maxY ? maxY : imageView.frame.maxY
            }
        }
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: viewW, height: viewH))
        view.backgroundColor = THEME_COLOR_WHITE
        
        var imageViewMaxX = CGFloat.min
        var imageViewMaxY = CGFloat.min
        var imageViewMinX = CGFloat.max
        var imageViewMinY = CGFloat.max
        
        for imageView in imageViews {
            imageView.center = CGPoint(x: imageView.center.x - minX + THEME_VIEW_MINIMUM_SPACE, y: imageView.center.y - minY + THEME_VIEW_MINIMUM_SPACE)
            imageViewMaxX = imageView.frame.maxX > imageViewMaxX ? imageView.frame.maxX : imageViewMaxX
            imageViewMaxY = imageView.frame.maxY > imageViewMaxY ? imageView.frame.maxY : imageViewMaxY
            imageViewMinX = imageView.frame.minX < imageViewMinX ? imageView.frame.minX : imageViewMinX
            imageViewMinY = imageView.frame.minY < imageViewMinY ? imageView.frame.minY : imageViewMinY
            imageView.layer.borderWidth = 0
            imageView.alpha = 1
            view.addSubview(imageView)
        }
        
        for imageView in imageViews {
            imageView.center = CGPoint(x: imageView.center.x + (view.bounds.width - imageViewMaxX - imageViewMinX) / 2, y: imageView.center.y + (view.bounds.height - imageViewMaxY - imageViewMinY) / 2)
        }
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0.0)
        view.drawViewHierarchyInRect(CGRect(x: 0, y: 0, width: view.bounds.width + 1, height: view.bounds.height + 1), afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    }
    
    static func getAutomaticRatioView(imageViews: [UIImageView], backgroundView: UIView) {
        var minX: CGFloat = CGFloat.max
        var minY: CGFloat = CGFloat.max
        var maxX: CGFloat = CGFloat.min
        var maxY: CGFloat = CGFloat.min
        for imageView in imageViews {
            minX = imageView.frame.origin.x > minX ? minX : imageView.frame.origin.x
            minY = imageView.frame.origin.y > minY ? minY : imageView.frame.origin.y
            maxX = imageView.frame.maxX < maxX ? maxX : imageView.frame.maxX
            maxY = imageView.frame.maxY < maxY ? maxY : imageView.frame.maxY
        }
        
        var viewW: CGFloat = 0
        var viewH: CGFloat = 0
        if maxX - minX > maxY - minY {
            viewW = maxX - minX + 2 * THEME_VIEW_MINIMUM_SPACE
            viewH = maxX - minX + 2 * THEME_VIEW_MINIMUM_SPACE
        } else {
            viewW = maxY - minY + 2 * THEME_VIEW_MINIMUM_SPACE
            viewH = maxY - minY + 2 * THEME_VIEW_MINIMUM_SPACE
        }
        
        var imageViewMaxX = CGFloat.min
        var imageViewMaxY = CGFloat.min
        var imageViewMinX = CGFloat.max
        var imageViewMinY = CGFloat.max
        
        for imageView in imageViews {
            imageView.center = CGPoint(x: imageView.center.x - minX + THEME_VIEW_MINIMUM_SPACE, y: imageView.center.y - minY + THEME_VIEW_MINIMUM_SPACE)
            imageViewMaxX = imageView.frame.maxX > imageViewMaxX ? imageView.frame.maxX : imageViewMaxX
            imageViewMaxY = imageView.frame.maxY > imageViewMaxY ? imageView.frame.maxY : imageViewMaxY
            imageViewMinX = imageView.frame.minX < imageViewMinX ? imageView.frame.minX : imageViewMinX
            imageViewMinY = imageView.frame.minY < imageViewMinY ? imageView.frame.minY : imageViewMinY
            imageView.layer.borderWidth = 0
            imageView.alpha = 1
        }
        
        for imageView in imageViews {
            imageView.bounds = CGRect(x: 0, y: 0, width: imageView.bounds.width * (backgroundView.bounds.width / viewW), height: imageView.bounds.height * (backgroundView.bounds.width / viewW))
            imageView.center = CGPoint(x: (imageView.center.x + (viewW - imageViewMaxX - imageViewMinX) / 2) * (backgroundView.bounds.width / viewW), y: (imageView.center.y + (viewH - imageViewMaxY - imageViewMinY) / 2) * (backgroundView.bounds.width / viewW) + (backgroundView.bounds.height - (viewW * (backgroundView.bounds.width / viewW))) / 2)
        }
    }
    
}