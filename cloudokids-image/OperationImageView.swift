//
//  OperationImageView.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/7/29.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

class OperationImageView: UIImageView {
    
    let gesture = GestureRecognizer()
    var isLocking: Bool = false {
        didSet {
            gesture.isLocking = isLocking
        }
    }
    weak var delegate: OperationImageViewDelegate?
    
    var backgroundView: UIView? {
        didSet {
            gesture.deletage = self
            gesture.backgroundView = backgroundView
            backgroundView?.addSubview(self)
            if let view = backgroundView {
                center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
            }
            setupGestureRecognizer()
        }
    }

    override init(image: UIImage?) {
        super.init(image: image)
        userInteractionEnabled = true
        let imageScreenW: CGFloat = 160.0
        let imageScreenH: CGFloat = 160.0

        if let image = image {
            var photoW: CGFloat = 0
            var photoH: CGFloat = 0
            if image.size.width > image.size.height {
                photoW = imageScreenW
                photoH = image.size.height / image.size.width * imageScreenW
            } else {
                photoH = imageScreenH
                photoW = image.size.width / image.size.height * imageScreenH
            }
            frame = CGRectMake(0, 0, photoW, photoH)
        }
        
        layer.shouldRasterize = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupGestureRecognizer() {
        gesture.addPanGestureRecognizer(self)
        gesture.addPinchGestureRecognizer(self)
        gesture.addRotationGestureRecognizer(self)
        gesture.addTapGestureRecognizer(self)
    }

}

extension OperationImageView: GestureRecognizerDelegate {
    
    func clickImage() {
        if let _ = delegate {
            delegate?.clickImage(self)
        }
    }
    
    func pictureChange() {
        if let _ = delegate {
            delegate?.viewPictureChange()
        }
    }
    
}

protocol OperationImageViewDelegate: NSObjectProtocol {
    func clickImage(imageView: OperationImageView)
    func viewPictureChange()
}
