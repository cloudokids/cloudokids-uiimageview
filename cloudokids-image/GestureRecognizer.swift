//
//  GestureRecognizer.swift
//  cloudokids-image
//
//  Created  kby 浦源 on 16/7/29.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

class GestureRecognizer: NSObject {
    
    var backgroundView: UIView?
    var view: UIView?
    var isLocking: Bool = false
    var isTap: Bool = false
    weak var deletage: GestureRecognizerDelegate?
    weak var viewDelegate: GestureRecognizerViewDelegate?
    
    func addPinchGestureRecognizer(view: UIView) {
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(GestureRecognizer.scale(_:)))
        pinchRecognizer.delegate = self
        view.addGestureRecognizer(pinchRecognizer)
    }
    
    func scale(pr: UIPinchGestureRecognizer){
        guard !isLocking else { return }
        
        if let view = self.view {
            isViewScale(view, pr: pr)
        }
    }
    
    func isViewScale(view: UIView, pr: UIPinchGestureRecognizer) {
        if let _ = viewDelegate where pr.state == .Changed {
            viewDelegate?.pictureChange()
        }
        
        if (pr.state == UIGestureRecognizerState.Began){
            _ = pr.locationInView(pr.view)
        }
        
        if view.frame.width > 600 {
            if pr.scale > 1 {
                pr.scale = 1
            }
        } else if view.frame.width < 5 {
            if pr.scale < 1 {
                pr.scale = 1
            }
        }
        view.bounds = CGRect(x: 0, y: 0, width: view.bounds.width * pr.scale, height: view.bounds.height * pr.scale)
        
        pr.scale = 1
    }
    
    func addRotationGestureRecognizer(targetView: UIView) {
        let rotationRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(GestureRecognizer.rotate(_:)))
        rotationRecognizer.delegate = self
        targetView.addGestureRecognizer(rotationRecognizer)
    }
    
    func rotate(pr: UIRotationGestureRecognizer) {
        guard !isLocking else { return }
        
        if let view = self.view {
            isViewRotate(view, pr: pr)
        }
    }
    
    func isViewRotate(view: UIView, pr: UIRotationGestureRecognizer) {
        if let _ = viewDelegate where pr.state == .Changed {
            viewDelegate?.pictureChange()
        }
        view.transform = CGAffineTransformRotate(view.transform, pr.rotation)
        pr.rotation = 0
    }
    
    func addPanGestureRecognizer(targetView: UIView){
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(GestureRecognizer.move(_:)))
        panRecognizer.minimumNumberOfTouches = 1
        panRecognizer.maximumNumberOfTouches = 1
        panRecognizer.delegate = self
        targetView.addGestureRecognizer(panRecognizer)
    }
    
    func move(pr: UIPanGestureRecognizer) {
        guard !isLocking else { return }
        guard !isViewMove(pr) else { return }
        
        if let view = backgroundView {
            if let _ = deletage where pr.state == .Changed {
                deletage?.pictureChange()
            }
            
            let translatedPoint = pr.translationInView(view)
            var objectView: UIView?
            pr.setTranslation(CGPointZero, inView: view)
            
            if (pr.view!.isKindOfClass(UILabel)){
                objectView = pr.view as! UILabel
            } else if (pr.view!.isKindOfClass(UIImageView)){
                objectView = pr.view as! UIImageView
            }
            objectView!.center = CGPoint(x: objectView!.center.x + translatedPoint.x, y: objectView!.center.y + translatedPoint.y)
        }
        if pr.state == .Began {
            isTap = false
        } else if pr.state == .Ended {
            UIView.animateWithDuration(0.1, animations: { }, completion: { (_) in
                self.isTap = true
            })
        }
    }
    
    func isViewMove(pr: UIPanGestureRecognizer) -> Bool {
        if pr.state == .Began {
            isTap = false
        } else if pr.state == .Ended {
            UIView.animateWithDuration(0.1, animations: { }, completion: { (_) in
                self.isTap = true
            })
        }
        if let view = self.view {
            if let _ = viewDelegate where pr.state == .Changed {
                viewDelegate?.pictureChange()
            }
            let translatedPoint = pr.translationInView(pr.view)
            var objectView: UIView?
            pr.setTranslation(CGPointZero, inView: pr.view)
            
            if (view.isKindOfClass(UIImageView)){
                objectView = view as! UIImageView
            }
            
            objectView!.center = CGPoint(x: objectView!.center.x + translatedPoint.x, y: objectView!.center.y + translatedPoint.y)
            
            return true
        }
        
        return false
    }
    
    func addTapGestureRecognizer(targetView: UIView) {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(GestureRecognizer.selectImage))
        targetView.addGestureRecognizer(tapRecognizer)
    }
    
    func selectImage(pr: UITapGestureRecognizer) {
        print(pr.locationInView(backgroundView))
        if let _ = self.view where isTap {
            if let _ = viewDelegate {
                viewDelegate?.removeView()
            }
        } else if let _ = pr.view where isTap {
            if let _ = deletage {
                deletage?.clickImage()
            }
        }
        
        isTap = true
    }
    
}

extension GestureRecognizer: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

protocol GestureRecognizerDelegate: NSObjectProtocol {
    func clickImage()
    func pictureChange()
}

protocol GestureRecognizerViewDelegate: NSObjectProtocol {
    func removeView()
    func pictureChange()
}
