//
//  UIImageHelper.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/8/3.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

extension UIImage {
    
    func imageAtRect(rect: CGRect) -> UIImage {
        let imageRef = CGImageCreateWithImageInRect(self.CGImage, rect)
        let subImage = UIImage(CGImage: imageRef!)
        
        return subImage
    }
    
}