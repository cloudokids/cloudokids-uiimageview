//
//  ListPhotosViewController.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/8/2.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit
import Photos

class ListPhotosViewController: UIViewController {

    //storyboard
    @IBOutlet weak var listPhotosCollectionView: UICollectionView!
    
    //var
    lazy var assets = [PHAsset]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.enabled = true
        
        checkPermission()
    }
    
    func checkPermission() {
        let status = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        
        switch status {
        case .Authorized:
            fetchPhotos()
        case .NotDetermined:
            requestPermission()
        default:
            break
        }
    }
    
    private func fetchPhotos(completion: (() -> Void)? = nil) {
        Photos.fetch { [weak self] assets in
            if let _ = self {
                self!.assets.removeAll()
                self!.assets.appendContentsOf(assets)
                self!.listPhotosCollectionView.reloadData()
            }
            
            completion?()
        }
    }
    
    private func requestPermission() {
        AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo) { granted in
            dispatch_async(dispatch_get_main_queue()) { [weak self] in
                if granted {
                    if let _ = self {
                        self!.fetchPhotos()
                    }
                }
            }
        }
    }
    
}
