//
//  Config.swift
//  Cloudo Snapper
//
//  Created by Juncheng Ren on 15/06/2016.
//  Copyright © 2016 Cloudo Kids. All rights reserved.
//

let OPERATION_STRINGS = ["删除","翻转","透明","置前","置后","锁定/解锁"]
let OPERATION_IMAGE_NAME = ["Delete", "Rotate", "Copy", "Up", "Down", "Unlock"]