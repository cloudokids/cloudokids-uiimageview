//
//  ClipImageViewController.swift
//  cloudokids-image
//
//  Created by 浦源 on 16/8/2.
//  Copyright © 2016年 浦源. All rights reserved.
//

import UIKit

class ClipImageViewController: UIViewController {
    
    //var
    lazy var photoImage = UIImage()
    lazy var photoImageView = UIImageView()
    lazy var cropMaskView = UIView()
    lazy var cropView = UIView()
    
    lazy var firstCorner = UIImageView()
    lazy var secondCorner = UIImageView()
    lazy var thirdCorner = UIImageView()
    lazy var fourthCorner = UIImageView()
    lazy var firstView = UIView()
    lazy var secondView = UIView()
    lazy var thirdView = UIView()
    lazy var fourthView = UIView()
    
    let borderLayer = CAShapeLayer()
    var firstPoint: CGPoint?
    var secondPoint: CGPoint?
    var thirdPoint: CGPoint?
    var fourthPoint: CGPoint?
    var startPointCropView: CGPoint?
    var pinchOriginalSize = CGSize()
    var imageScale = CGFloat()
    var isBothClick: Int = 0
    var bothNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewController()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.enabled = false
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "image", object: nil)
    }
    
    func setupViewController() {
        setupPhotoImageView()
    }
    
    func setupPhotoImageView() {
        cropMaskView.frame = CGRectMake(0, 64, view.bounds.width, view.bounds.height - 64)
        cropMaskView.backgroundColor = THEME_COLOR_BLACK
        cropMaskView.alpha = 0.4
        
        let screenW = UIScreen.mainScreen().bounds.width
        let screenH = cropMaskView.frame.height
        
        var photoW = CGFloat()
        var photoH = CGFloat()
        
        if photoImage.size.width > photoImage.size.height || photoImage.size.width / photoImage.size.height > screenW / screenH {
            photoW = screenW
            photoH = photoImage.size.height / photoImage.size.width * screenW
        } else {
            photoH = screenH
            photoW = photoImage.size.width / photoImage.size.height * screenH
        }
        imageScale = photoImage.size.width / CGRectGetWidth(CGRectMake(0, 0, photoW, photoH))
        photoImageView.frame = CGRectMake(0, 0, photoW, photoH)
        photoImageView.image = photoImage
        photoImageView.center = cropMaskView.center
        view.addSubview(photoImageView)
        view.addSubview(cropMaskView)
        
        cropView.frame = photoImageView.frame
        
        borderLayerBounds()
        borderLayer.lineWidth = THEME_CROPVIEW_BORDER_WIDTH
        borderLayer.lineDashPattern = [8, 8]
        borderLayer.fillColor = nil
        borderLayer.strokeColor = THEME_COLOR_WHITE.CGColor
        
        cropView.layer.addSublayer(borderLayer)
        cropView.backgroundColor = THEME_COLOR_CLEAR
        view.addSubview(cropView)
        
        setupCornerFrame()
        setupViewFrame()
        setupCorner()
        resetCropMask()
    }
    
    func setupCorner() {
        firstView.userInteractionEnabled = true
        secondView.userInteractionEnabled = true
        thirdView.userInteractionEnabled = true
        fourthView.userInteractionEnabled = true
        firstCorner.userInteractionEnabled = true
        secondCorner.userInteractionEnabled = true
        thirdCorner.userInteractionEnabled = true
        fourthCorner.userInteractionEnabled = true
        firstView.backgroundColor = UIColor.clearColor()
        secondView.backgroundColor = UIColor.clearColor()
        thirdView.backgroundColor = UIColor.clearColor()
        fourthView.backgroundColor = UIColor.clearColor()
        
        firstCorner.image = UIImage(named: "arrow1")
        secondCorner.image = UIImage(named: "arrow2")
        thirdCorner.image = UIImage(named: "arrow3")
        fourthCorner.image = UIImage(named: "arrow4")
        
        addPanGestureRecognizer(firstCorner)
        addPanGestureRecognizer(secondCorner)
        addPanGestureRecognizer(thirdCorner)
        addPanGestureRecognizer(fourthCorner)
        addPanGestureRecognizer(firstView)
        addPanGestureRecognizer(secondView)
        addPanGestureRecognizer(thirdView)
        addPanGestureRecognizer(fourthView)
        
        view.addSubview(firstCorner)
        view.addSubview(secondCorner)
        view.addSubview(thirdCorner)
        view.addSubview(fourthCorner)
        cropView.addSubview(firstView)
        cropView.addSubview(secondView)
        cropView.addSubview(thirdView)
        cropView.addSubview(fourthView)
    }
    
    //四个角的frame
    func setupCornerFrame() {
        firstCorner.frame = CGRectMake(CGRectGetMinX(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH, CGRectGetMinY(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH, THEME_ARROW_WIDTH, THEME_ARROW_HEIGHT)
        secondCorner.frame = CGRectMake(CGRectGetMaxX(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_WIDTH, CGRectGetMinY(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH, THEME_ARROW_WIDTH, THEME_ARROW_HEIGHT)
        thirdCorner.frame = CGRectMake(CGRectGetMinX(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH, CGRectGetMaxY(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_HEIGHT, THEME_ARROW_WIDTH, THEME_ARROW_HEIGHT)
        fourthCorner.frame = CGRectMake(CGRectGetMaxX(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_WIDTH, CGRectGetMaxY(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_HEIGHT, THEME_ARROW_WIDTH, THEME_ARROW_HEIGHT)
        
        view.layoutIfNeeded()
    }
    
    func setupViewFrame() {
        firstView.frame = CGRectMake(0, 0, cropView.frame.width / 2, cropView.bounds.height / 2)
        secondView.frame = CGRectMake(cropView.bounds.width / 2, 0, cropView.bounds.width / 2, cropView.bounds.height / 2)
        thirdView.frame = CGRectMake(0, cropView.bounds.height / 2, cropView.bounds.width / 2, cropView.bounds.height / 2)
        fourthView.frame = CGRectMake(cropView.bounds.width / 2, cropView.bounds.height / 2, cropView.bounds.width / 2, cropView.bounds.height / 2)
    }
    
    func setupPoint(pr: UIPanGestureRecognizer, startPoint: CGPoint) {
        if pr.view == firstCorner || pr.view == firstView {
            firstPoint = startPoint
        } else if pr.view == secondCorner || pr.view == secondView {
            secondPoint = startPoint
        } else if pr.view == thirdCorner || pr.view == thirdView {
            thirdPoint = startPoint
        } else if pr.view == fourthCorner || pr.view == fourthView {
            fourthPoint = startPoint
        }
    }
    
    func borderLayerBounds() {
        borderLayer.bounds = cropView.bounds
        borderLayer.position = CGPoint(x: CGRectGetMidX(cropView.bounds), y: CGRectGetMidY(cropView.bounds))
        borderLayer.path = UIBezierPath(rect: borderLayer.bounds).CGPath
    }
    
    func resetArrowsFollow(arrow: UIView) {
        if arrow == firstCorner {
            secondCorner.center = CGPointMake(secondCorner.center.x, firstCorner.center.y)
            thirdCorner.center = CGPointMake(firstCorner.center.x, thirdCorner.center.y)
            return
        } else if arrow == secondCorner {
            firstCorner.center = CGPointMake(firstCorner.center.x, secondCorner.center.y)
            fourthCorner.center = CGPointMake(secondCorner.center.x, fourthCorner.center.y)
            return
        } else if arrow == thirdCorner {
            firstCorner.center = CGPointMake(thirdCorner.center.x, firstCorner.center.y)
            fourthCorner.center = CGPointMake(fourthCorner.center.x, thirdCorner.center.y)
            return
        } else if arrow == fourthCorner {
            secondCorner.center = CGPointMake(fourthCorner.center.x, secondCorner.center.y)
            thirdCorner.center = CGPointMake(thirdCorner.center.x, fourthCorner.center.y)
            return
        }
    }
    
    //根据当前裁剪区域的位置和尺寸将黑色蒙板的相应区域抠成透明
    func resetCropMask() {
        let path = UIBezierPath(rect: cropMaskView.bounds)
        let clearPath = UIBezierPath(rect: CGRectMake(CGRectGetMinX(cropView.frame) + THEME_CROPVIEW_BORDER_WIDTH, CGRectGetMinY(cropView.frame) + THEME_CROPVIEW_BORDER_WIDTH - 64 , CGRectGetWidth(cropView.frame) - 2 *  THEME_CROPVIEW_BORDER_WIDTH, CGRectGetHeight(cropView.frame) - 2 * THEME_CROPVIEW_BORDER_WIDTH))
        let a = clearPath.bezierPathByReversingPath()
        path.appendPath(a)
        
        if let mask = cropMaskView.layer.mask as? CAShapeLayer {
            mask.path = path.CGPath
        } else {
            let shapeLayer = CAShapeLayer()
            cropMaskView.layer.mask = shapeLayer
            shapeLayer.path = path.CGPath
        }
    }
    
    func resetCropView() {
        cropView.frame = CGRectMake(CGRectGetMinX(firstCorner.frame) + THEME_CROPVIEW_BORDER_WIDTH, CGRectGetMinY(firstCorner.frame) + THEME_CROPVIEW_BORDER_WIDTH, CGRectGetMaxX(secondCorner.frame) - CGRectGetMinX(firstCorner.frame) - THEME_CROPVIEW_BORDER_WIDTH * 2, CGRectGetMaxY(thirdCorner.frame) - CGRectGetMinY(firstCorner.frame) - THEME_CROPVIEW_BORDER_WIDTH * 2)
        setupViewFrame()
    }
    
    func resetAllArrows() {
        firstCorner.center = CGPoint(x: CGRectGetMinX(cropView.frame) - THEME_CROPVIEW_BORDER_WIDTH + THEME_ARROW_WIDTH * 0.5, y: CGRectGetMinY(cropView.frame) - THEME_CROPVIEW_BORDER_WIDTH + THEME_ARROW_HEIGHT * 0.5)
        secondCorner.center = CGPoint(x: CGRectGetMaxX(cropView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_WIDTH * 0.5, y: CGRectGetMinY(cropView.frame) - THEME_CROPVIEW_BORDER_WIDTH + THEME_ARROW_HEIGHT * 0.5)
        thirdCorner.center = CGPoint(x: CGRectGetMinX(cropView.frame) - THEME_CROPVIEW_BORDER_WIDTH + THEME_ARROW_WIDTH * 0.5, y: CGRectGetMaxY(cropView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_HEIGHT * 0.5)
        fourthCorner.center = CGPoint(x: CGRectGetMaxX(cropView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_WIDTH * 0.5, y: CGRectGetMaxY(cropView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_HEIGHT * 0.5)
        
        view.layoutIfNeeded()
    }
    
    func addPanGestureRecognizer(view: UIView) {
        let panRecognizer = UIPanGestureRecognizer()
        if view == firstView || view == secondView || view == thirdView || view == fourthView {
            panRecognizer.addTarget(self, action: #selector(ClipImageViewController.moveView(_:)))
        } else {
            panRecognizer.addTarget(self, action: #selector(ClipImageViewController.move(_:)))
        }
        panRecognizer.minimumNumberOfTouches = 1
        panRecognizer.maximumNumberOfTouches = 1
        view.addGestureRecognizer(panRecognizer)
    }
    
    func moveCropView(panGesture: UIPanGestureRecognizer) {
        let minX = CGRectGetMinX(photoImageView.frame)
        let maxX = CGRectGetMaxX(photoImageView.frame) - CGRectGetWidth(cropView.frame)
        let minY = CGRectGetMinY(photoImageView.frame)
        let maxY = CGRectGetMaxY(photoImageView.frame) - CGRectGetHeight(cropView.frame)
        
        switch panGesture.state {
        case .Began:
            startPointCropView = panGesture.locationInView(cropMaskView)
        case .Changed:
            let endPoint = panGesture.locationInView(cropMaskView)
            if bothNumber > isBothClick {
                bothNumber = isBothClick
                startPointCropView = panGesture.locationInView(cropMaskView)
            }
            var frame = cropView.frame
            if let point = startPointCropView {
                frame.origin.x += endPoint.x - point.x
                frame.origin.y += endPoint.y - point.y
                frame.origin.x = min(maxX, max(frame.origin.x, minX))
                frame.origin.y = min(maxY, max(frame.origin.y, minY))
                cropView.frame = frame
                startPointCropView = endPoint
            }
        default:
            break
        }
        
        resetCropMask()
        resetAllArrows()
        borderLayerBounds()
    }
    
    //四个角拖动执行的方法
    func move(pr: UIPanGestureRecognizer) {
        var startPoint: CGPoint?
        var minX = CGRectGetMinX(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH
        var maxX = CGRectGetMaxX(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_WIDTH
        var minY = CGRectGetMinY(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH
        var maxY = CGRectGetMaxY(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_HEIGHT
        
        if pr.view == firstCorner {
            startPoint = firstPoint
            maxY = CGRectGetMinY(thirdCorner.frame) - THEME_ARROW_HEIGHT - THEME_ARROW_MINIMUM_SPACE
            maxX = CGRectGetMinX(secondCorner.frame) - THEME_ARROW_WIDTH - THEME_ARROW_MINIMUM_SPACE
        } else if pr.view == secondCorner {
            startPoint = secondPoint
            maxY = CGRectGetMinY(fourthCorner.frame) - THEME_ARROW_HEIGHT - THEME_ARROW_MINIMUM_SPACE
            minX = CGRectGetMaxX(firstCorner.frame) + THEME_ARROW_MINIMUM_SPACE
        } else if pr.view == thirdCorner {
            startPoint = thirdPoint
            minY = CGRectGetMaxY(firstCorner.frame) + THEME_ARROW_MINIMUM_SPACE
            maxX = CGRectGetMinX(fourthCorner.frame) - THEME_ARROW_WIDTH - THEME_ARROW_MINIMUM_SPACE
        } else if pr.view == fourthCorner {
            startPoint = fourthPoint
            minY = CGRectGetMaxY(secondCorner.frame) + THEME_ARROW_MINIMUM_SPACE
            minX = CGRectGetMaxX(thirdCorner.frame) + THEME_ARROW_MINIMUM_SPACE
        }
        
        switch pr.state {
        case .Began:
            startPoint = pr.locationInView(cropMaskView)
            if let _ = startPoint {
                setupPoint(pr, startPoint: startPoint!)
            }
        case .Changed:
            let endPoint = pr.locationInView(cropMaskView)
            var frame = pr.view?.frame
            if let _ = frame, point = startPoint {
                frame?.origin.x += endPoint.x - point.x
                frame?.origin.y += endPoint.y - point.y
                frame?.origin.x = min(maxX, max(frame!.origin.x, minX))
                frame?.origin.y = min(maxY, max(frame!.origin.y, minY))
                pr.view?.frame = frame!
                startPoint = endPoint
                setupPoint(pr, startPoint: startPoint!)
            }
        default:
            break
        }
        
        if let view = pr.view {
            resetArrowsFollow(view)
        }
        resetCropView()
        resetCropMask()
        borderLayerBounds()
    }
    
    func moveView(pr: UIPanGestureRecognizer) {
        var startPoint: CGPoint?
        var cornerView = UIImageView()
        var frame = CGRect()
        let endPoint = pr.locationInView(cropMaskView)
        
        var minX = CGRectGetMinX(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH
        var maxX = CGRectGetMaxX(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_WIDTH
        var minY = CGRectGetMinY(photoImageView.frame) - THEME_CROPVIEW_BORDER_WIDTH
        var maxY = CGRectGetMaxY(photoImageView.frame) + THEME_CROPVIEW_BORDER_WIDTH - THEME_ARROW_HEIGHT
        
        if pr.view == firstView {
            startPoint = firstPoint
            cornerView = firstCorner
            frame = firstCorner.frame
            maxY = CGRectGetMinY(thirdCorner.frame) - THEME_ARROW_HEIGHT - THEME_ARROW_MINIMUM_SPACE
            maxX = CGRectGetMinX(secondCorner.frame) - THEME_ARROW_WIDTH - THEME_ARROW_MINIMUM_SPACE
        } else if pr.view == secondView {
            startPoint = secondPoint
            cornerView = secondCorner
            frame = secondCorner.frame
            maxY = CGRectGetMinY(fourthCorner.frame) - THEME_ARROW_HEIGHT - THEME_ARROW_MINIMUM_SPACE
            minX = CGRectGetMaxX(firstCorner.frame) + THEME_ARROW_MINIMUM_SPACE
        } else if pr.view == thirdView {
            startPoint = thirdPoint
            cornerView = thirdCorner
            frame = thirdCorner.frame
            minY = CGRectGetMaxY(firstCorner.frame) + THEME_ARROW_MINIMUM_SPACE
            maxX = CGRectGetMinX(fourthCorner.frame) - THEME_ARROW_WIDTH - THEME_ARROW_MINIMUM_SPACE
        } else if pr.view == fourthView {
            startPoint = fourthPoint
            cornerView = fourthCorner
            frame = fourthCorner.frame
            minY = CGRectGetMaxY(secondCorner.frame) + THEME_ARROW_MINIMUM_SPACE
            minX = CGRectGetMaxX(thirdCorner.frame) + THEME_ARROW_MINIMUM_SPACE
        }
        
        switch pr.state {
        case .Began:
            isBothClick += 1
            bothNumber = isBothClick
            startPoint = pr.locationInView(cropMaskView)
            if let _ = startPoint {
                setupPoint(pr, startPoint: startPoint!)
            }            
        case .Ended:
            isBothClick -= 1
            if isBothClick == 0 {
                bothNumber = 0
            }
        default:
            break
        }
        if isBothClick < 2 {
            moveCropView(pr)
            return
        }
        
        switch pr.state {
        case .Changed:
            setupPoint(pr, startPoint: endPoint)
            if let point = startPoint {
                frame.origin.x += endPoint.x - point.x
                frame.origin.y += endPoint.y - point.y
                frame.origin.x = min(maxX, max(frame.origin.x, minX))
                frame.origin.y = min(maxY, max(frame.origin.y, minY))
                
                cornerView.frame = frame
            }
        default:
            break
        }
        
        resetArrowsFollow(cornerView)
        resetCropView()
        resetCropMask()
        borderLayerBounds()
    }
    
    func cropAreaInImage() -> CGRect {
        let cropAreaInImageView = cropMaskView.convertRect(cropView.frame, toView: photoImageView)
        var cropAreaInImage = CGRect()
        cropAreaInImage.origin.x = cropAreaInImageView.origin.x * imageScale
        cropAreaInImage.origin.y = (cropAreaInImageView.origin.y - 64) * imageScale
        cropAreaInImage.size.width = cropAreaInImageView.size.width * imageScale
        cropAreaInImage.size.height = cropAreaInImageView.size.height * imageScale
        return cropAreaInImage
    }
    
    @IBAction func confirm(sender: UIBarButtonItem) {
        cropView.layer.borderWidth = 0
        let image = photoImage.imageAtRect(cropAreaInImage())
        let dict = ["image":image]
        
        NSNotificationCenter.defaultCenter().postNotificationName("image", object: dict)
        
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
}
